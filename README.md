# Docker compose with full satnogs-client stack

0. To use UHD radios execute: `sudo wget https://raw.githubusercontent.com/EttusResearch/uhd/master/host/utils/uhd-usrp.rules -P /etc/udev/rules.d/ && sudo udevadm control --reload-rules`
1. Clone this repo
2. `git submodule update --recursive --init`
3. Configure satnogs-client (for now `gcs-satnogs-client/opt/satnogs-client/satnogs-client.conf`)
4. `docker-compose up -d`

# Satnogs-client configuration
Described in gcs-satnogs-client README.md

# Connection between satnogs-client and rigctld
Satnogs-flowgraphs has hardcoded IP of rigctld(127.0.0.1).
Calls to 127.0.0.1:RIGCTLD\_PORT can be forwarded to rigctld container using socat.
To turn socat on pass enviroment variables to satnogs-client container in docker-compose.yaml.

| Variable name | Description |
| --- | --- |
| RIGCTLD\_HOST | Hostname/IP of rigctld container |
| RIGCTLD\_PORT | Rigctld listen port exposed by rigctld container |
